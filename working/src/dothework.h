/**
  * 'dothework.h'
  *
  * Author/CopyRight: Mancuso, Logan
  * Last Edit Date: 08-22-2019--14:30:49
  *
 **/


#include "../util/utils.h"
#include "../util/scanner.h"
#include "../util/scanline.h"


class DoTheWork {
  public:
    DoTheWork();
    virtual ~DoTheWork();
    void begin(std::ofstream* log_stream, Scanner* input_stream, std::ofstream* out_stream);
  private:
};



 /**
  * End 'dothework.h'
 **/